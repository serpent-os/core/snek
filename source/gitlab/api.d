/* SPDX-License-Identifier: Zlib */

/**
 * Internal API wrapping for gitlab
 *
 * Authors: © 2022 Serpent OS Developers
 * License: ZLib
 */

module gitlab.api;
