/* SPDX-License-Identifier: Zlib */

/**
 * GitLab API encapsulation
 *
 * Provides some methods with which to interact with the
 * GitLab APIs
 *
 * Authors: © 2022 Serpent OS Developers
 * License: ZLib
 */

module gitlab;

import std.experimental.logger;
import std.string : format;
import std.net.curl;
import std.json;
import std.parallelism;
import std.process;
import std.exception : enforce;
import std.stdio : stdin, stdout, stderr;

/**
 * Default gitlab instance to interact with is the public
 * instance where the Serpent OS code is stored
 */

public immutable(string) defaultInstance = "https://gitlab.com";

/**
 * Client access for GitLab
 */
public final class GitlabClient
{
    /**
     * Construct a new GitlabClient with the given API base URI
     */
    this(in string instance = defaultInstance)
    {
        _instanceURI = instance;
        apiURI = format!"%s/api/v4"(_instanceURI);
    }

    /**
     * Attempt to connect to GitLab
     */
    void connect()
    {
        struct GitProject
        {
            string sshURL;
            string httpURL;
            string name;
        }

        GitProject[] projects;
        auto content = get(format!"%s/groups/12403989/projects?simple=false"(apiURI));
        auto js = parseJSON(content);
        foreach (node; js.array)
        {
            auto name = node["name"].get!string;
            if (name == "snek" || name == "serpent-style")
            {
                continue;
            }
            auto sshPath = node["ssh_url_to_repo"].get!string;
            auto httpsPath = node["http_url_to_repo"].get!string;
            projects ~= GitProject(sshPath, httpsPath, name);
        }

        foreach (p; projects.parallel)
        {
            auto c = Config.inheritFDs | Config.retainStderr
                | Config.retainStdin | Config.retainStdout;
            auto cmd = spawnProcess(["git", "clone", p.httpURL,], stdin, stdin, stderr, null, c);
            auto status = cmd.wait();
            enforce(status == 0, format!"Clone failed of %s"(p.name));
            cmd = spawnProcess([
                "git", "-C", p.name, "submodule", "update", "--init",
                "--recursive",
            ], stdin, stdout, stderr, null, c);
            status = cmd.wait();
            enforce(status == 0, format!"Submodules failed for %s"(p.name));
        }
    }

    /**
     * Close the underlying connection to the gitlab instance
     */
    void close() @safe
    {
        warning("Not yet implemented");
    }

private:

    string _instanceURI;
    string apiURI;
}
