/* SPDX-License-Identifier: Zlib */

/**
 * Main entry
 *
 * Authors: © 2022 Serpent OS Developers
 * License: ZLib
 */

import std.stdio;

import std.experimental.logger;

import gitlab;

void main()
{
    auto client = new GitlabClient();
    scope (exit)
    {
        client.close();
    }
    client.connect();
    info("Testing development");
}
